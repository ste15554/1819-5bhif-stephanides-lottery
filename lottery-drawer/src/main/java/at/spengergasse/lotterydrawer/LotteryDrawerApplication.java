package at.spengergasse.lotterydrawer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication (scanBasePackages = {"at.spengergasse.lotterydrawer"})
@EnableFeignClients
@EnableEurekaClient
@EnableCircuitBreaker
public class LotteryDrawerApplication {

    public static void main(String[] args) {
        SpringApplication.run(LotteryDrawerApplication.class, args);
    }

}
