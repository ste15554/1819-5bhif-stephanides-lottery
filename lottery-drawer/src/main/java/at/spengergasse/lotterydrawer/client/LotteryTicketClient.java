package at.spengergasse.lotterydrawer.client;


import at.spengergasse.lotterydrawer.client.fallback.LotteryTicketFallback;
import at.spengergasse.lotterydrawer.model.LotteryTicket;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(value = "worker-service",fallbackFactory = LotteryTicketFallback.class)
public interface LotteryTicketClient {

    @GetMapping("/tickets")
    List<LotteryTicket> getAllTickets();

    @PostMapping("/addTicket")
    LotteryTicket addTicket(@RequestBody LotteryTicket newTicket);

    @DeleteMapping("/deleteTicket")
    boolean deleteTicket(@RequestBody LotteryTicket ticket);

    @DeleteMapping("/deleteTicketById")
    boolean deleteTicketById(@RequestBody long ticketId);

    @PostMapping("/generateTickets")
    List<LotteryTicket> generateTickets(int amount);

    @GetMapping("/active-tickets")
    List<LotteryTicket> getAllActiveTickets();

    @PostMapping("/save-all")
    void saveAllTickets(List<LotteryTicket> tickets);
}
