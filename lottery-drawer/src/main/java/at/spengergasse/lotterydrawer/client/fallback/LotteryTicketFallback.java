package at.spengergasse.lotterydrawer.client.fallback;

import at.spengergasse.lotterydrawer.client.LotteryTicketClient;
import at.spengergasse.lotterydrawer.model.LotteryTicket;

import java.util.List;

public class LotteryTicketFallback implements LotteryTicketClient {
    @Override
    public List<LotteryTicket> getAllTickets() {
        return null;
    }

    @Override
    public LotteryTicket addTicket(LotteryTicket newTicket) {
        return null;
    }

    @Override
    public boolean deleteTicket(LotteryTicket ticket) {
        return false;
    }

    @Override
    public boolean deleteTicketById(long ticketId) {
        return false;
    }

    @Override
    public List<LotteryTicket> generateTickets(int amount) {
        return null;
    }

    @Override
    public List<LotteryTicket> getAllActiveTickets() {
        return null;
    }

    @Override
    public void saveAllTickets(List<LotteryTicket> tickets) {

    }
}
