package at.spengergasse.lotterydrawer.controller;

import at.spengergasse.lotterydrawer.model.LotteryDrawResult;
import at.spengergasse.lotterydrawer.model.LotteryStartDraw;
import at.spengergasse.lotterydrawer.service.LotteryDrawService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LotteryController {

    private final LotteryDrawService lotteryDrawService;

    @Autowired
    public LotteryController(LotteryDrawService lotteryDrawService) {
        this.lotteryDrawService = lotteryDrawService;
    }

    @PostMapping("/draw")
    public LotteryDrawResult draw(@RequestBody LotteryStartDraw startDraw){
        return this.lotteryDrawService.draw(startDraw);
    }

    @GetMapping("/draws")
    public List<LotteryDrawResult> getDraws(){
        return this.lotteryDrawService.getDraws();
    }

}
