package at.spengergasse.lotterydrawer.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class LotteryDrawResult {

    @javax.persistence.Id
    @GeneratedValue
    private Long id;
    private int pot;
    @OneToMany(targetEntity=LotteryTicket.class)
    private List<LotteryTicket> winners;
    private int[] numbers;
    private Date date;

}
