package at.spengergasse.lotterydrawer.repository;

import at.spengergasse.lotterydrawer.model.LotteryDrawResult;
import at.spengergasse.lotterydrawer.model.LotteryTicket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LotteryDrawRepository extends JpaRepository<LotteryDrawResult,Long> {

}
