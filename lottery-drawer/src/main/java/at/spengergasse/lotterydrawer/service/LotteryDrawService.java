package at.spengergasse.lotterydrawer.service;

import at.spengergasse.lotterydrawer.client.LotteryTicketClient;
import at.spengergasse.lotterydrawer.model.LotteryDrawResult;
import at.spengergasse.lotterydrawer.model.LotteryStartDraw;
import at.spengergasse.lotterydrawer.model.LotteryTicket;
import at.spengergasse.lotterydrawer.repository.LotteryDrawRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.IntStream;

@Service
@Transactional
public class LotteryDrawService {

    private final LotteryDrawRepository drawRepository;
    private final LotteryTicketClient ticketClient;

    @Autowired
    public LotteryDrawService(LotteryDrawRepository drawRepository, LotteryTicketClient ticketClient) {
        this.drawRepository = drawRepository;
        this.ticketClient = ticketClient;
    }

    public LotteryDrawResult draw(LotteryStartDraw startDraw) {
        LotteryDrawResult result = new LotteryDrawResult();

        List<LotteryTicket> tickets = this.ticketClient.getAllActiveTickets();
        Random random = new Random(System.currentTimeMillis());

        result.setPot(startDraw.getPot());
        result.setDate(new Date());
        result.setWinners(new ArrayList<>());
        result.setNumbers(IntStream.generate(() -> random.nextInt(44) + 1).limit(6).toArray());

        Map<Integer, Integer> winnerAmounts = new HashMap<>();
        winnerAmounts.put(3, 0);
        winnerAmounts.put(4, 0);
        winnerAmounts.put(5, 0);
        winnerAmounts.put(6, 0);

        // Check who won
        for (LotteryTicket ticket : tickets) {
            ticket.setActive(false);

            int matching = matching(result.getNumbers(), ticket.getNumbers());
            if(matching > 2){
                winnerAmounts.put(matching, winnerAmounts.get(matching) + 1);
                result.getWinners().add(ticket);
            }
        }

        // Calculate winnings
        Map<Integer, Integer> winnerPrizes = new HashMap<>();
        // 6: 50%  5: 30%, 4: 15%, 3: 5%
        winnerPrizes.put(3, winnerAmounts.get(3) > 0 ? ((int)(startDraw.getPot() * 0.05) / winnerAmounts.get(3)) : 0);
        winnerPrizes.put(4, winnerAmounts.get(4) > 0 ? ((int)(startDraw.getPot() * 0.15) / winnerAmounts.get(4)) : 0);
        winnerPrizes.put(5, winnerAmounts.get(5) > 0 ? ((int)(startDraw.getPot() * 0.3) / winnerAmounts.get(5)) : 0);
        winnerPrizes.put(6, winnerAmounts.get(6) > 0 ? ((int)(startDraw.getPot() * 0.5) / winnerAmounts.get(6)) : 0);

        // Assign winnings
        for (LotteryTicket winner : result.getWinners()) {
            int matching = matching(result.getNumbers(), winner.getNumbers());
            winner.setPrizeMoney(winnerPrizes.get(matching));
        }

        this.ticketClient.saveAllTickets(tickets);

        this.drawRepository.save(result);
        return result;
    }

    public List<LotteryDrawResult> getDraws() {
        return this.drawRepository.findAll();
    }


    private int matching(int[] array1, int[] array2) {
        int matchCount = 0;

        for (int i = 0; i < array1.length; i++) {
            if(array1[i] == array2[i]) matchCount++;
        }

        return matchCount;
    }
}
