package at.spengergasse.lotteryeureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;


@EnableEurekaServer
@SpringBootApplication
@EnableHystrixDashboard
public class LotteryEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(LotteryEurekaApplication.class, args);
	}

}
