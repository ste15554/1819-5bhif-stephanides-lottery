package at.spengergasse.lotteryrest.client;


import at.spengergasse.lotteryrest.client.fallback.LotteryTicketFallback;
import at.spengergasse.lotteryrest.model.LotteryDrawResult;
import at.spengergasse.lotteryrest.model.LotteryStartDraw;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(value = "draw-service",fallbackFactory = LotteryTicketFallback.class)
public interface LotteryDrawClient {

    @PostMapping("/draw")
    LotteryDrawResult draw(@RequestBody LotteryStartDraw draw);

    @GetMapping("/draws")
    List<LotteryDrawResult> getDraws();
}
