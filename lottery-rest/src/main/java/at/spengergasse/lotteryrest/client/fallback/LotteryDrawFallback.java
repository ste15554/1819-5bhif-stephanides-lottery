package at.spengergasse.lotteryrest.client.fallback;

import at.spengergasse.lotteryrest.client.LotteryDrawClient;
import at.spengergasse.lotteryrest.model.LotteryDrawResult;
import at.spengergasse.lotteryrest.model.LotteryStartDraw;
import at.spengergasse.lotteryrest.model.LotteryTicket;

import java.util.List;

public class LotteryDrawFallback implements LotteryDrawClient {

    @Override
    public LotteryDrawResult draw(LotteryStartDraw draw) {
        return null;
    }

    @Override
    public List<LotteryDrawResult> getDraws() {
        return null;
    }
}
