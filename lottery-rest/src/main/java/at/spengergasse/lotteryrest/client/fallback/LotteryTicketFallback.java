package at.spengergasse.lotteryrest.client.fallback;

import at.spengergasse.lotteryrest.client.LotteryTicketClient;
import at.spengergasse.lotteryrest.model.LotteryDrawResult;
import at.spengergasse.lotteryrest.model.LotteryStartDraw;
import at.spengergasse.lotteryrest.model.LotteryTicket;

import java.util.List;

public class LotteryTicketFallback implements LotteryTicketClient {
    @Override
    public List<LotteryTicket> getAllTickets() {
        return null;
    }

    @Override
    public LotteryTicket addTicket(LotteryTicket newTicket) {
        return null;
    }

    @Override
    public boolean deleteTicket(LotteryTicket ticket) {
        return false;
    }

    @Override
    public boolean deleteTicketById(long ticketId) {
        return false;
    }

    @Override
    public List<LotteryTicket> generateTickets(int amount) {
        return null;
    }

    @Override
    public LotteryTicket checkWin(long ticketId) {
        return null;
    }
}
