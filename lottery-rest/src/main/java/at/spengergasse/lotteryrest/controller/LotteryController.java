package at.spengergasse.lotteryrest.controller;

import at.spengergasse.lotteryrest.client.LotteryDrawClient;
import at.spengergasse.lotteryrest.client.LotteryTicketClient;
import at.spengergasse.lotteryrest.model.LotteryDrawResult;
import at.spengergasse.lotteryrest.model.LotteryStartDraw;
import at.spengergasse.lotteryrest.model.LotteryTicket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class LotteryController {

    private final LotteryTicketClient lotteryTicketClient;
    private final LotteryDrawClient lotteryDrawClient;

    @Autowired
    public LotteryController(LotteryTicketClient lotteryTicketClient, LotteryDrawClient lotteryDrawClient) {
        this.lotteryTicketClient = lotteryTicketClient;
        this.lotteryDrawClient = lotteryDrawClient;
    }

    @GetMapping("/tickets")
    public List<LotteryTicket> getAllTickets() {
        return this.lotteryTicketClient.getAllTickets();
    }

    @PostMapping("/addTicket")
    public LotteryTicket addTicket(@RequestBody LotteryTicket newTicket){
        return this.lotteryTicketClient.addTicket(newTicket);
    }

    @PostMapping("/generateTickets")
    public List<LotteryTicket> generateTickets(@RequestBody int amount){
        return this.lotteryTicketClient.generateTickets(amount);
    }

    @DeleteMapping("/deleteTicket")
    public boolean deleteTicket(@RequestBody LotteryTicket ticket){
        this.lotteryTicketClient.deleteTicket(ticket);
        return true;
    }

    @DeleteMapping("/deleteTicketById")
    public boolean deleteTicketById(@RequestBody long ticketId){
        this.lotteryTicketClient.deleteTicketById(ticketId);
        return true;
    }

    @PostMapping("/draw")
    public LotteryDrawResult draw(@RequestBody LotteryStartDraw startDraw){
        return this.lotteryDrawClient.draw(startDraw);
    }

    @GetMapping("/draws")
    public List<LotteryDrawResult> getDraws(){
        return this.lotteryDrawClient.getDraws();
    }

    @GetMapping("/checkWin")
    public LotteryTicket checkWin(@RequestBody long ticketId){
        return this.lotteryTicketClient.checkWin(ticketId);
    }

}
