package at.spengergasse.lotteryrest.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LotteryDrawResult {

    private Long id;
    private int pot;
    private List<LotteryTicket> winners;
    private int[] numbers;
    private Date date;

}
