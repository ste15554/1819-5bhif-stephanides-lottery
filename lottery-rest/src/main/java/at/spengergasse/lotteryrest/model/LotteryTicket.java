package at.spengergasse.lotteryrest.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LotteryTicket {

    private Long Id;
    private int[] numbers;
    private Date date;
    private boolean active;
    private int prizeMoney;

}
