package at.spengergasse.lotteryworker.controller;

import at.spengergasse.lotteryworker.model.LotteryTicket;
import at.spengergasse.lotteryworker.service.LotteryTicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class LotteryController {

    private final LotteryTicketService lotteryTicketService;

    @Autowired
    public LotteryController(LotteryTicketService lotteryTicketService) {
        this.lotteryTicketService = lotteryTicketService;
    }

    @GetMapping("/tickets")
    public List<LotteryTicket> getAllTickets() {
        return this.lotteryTicketService.getAllTickets();
    }

    @PostMapping("/addTicket")
    public LotteryTicket addTicket(@RequestBody LotteryTicket newTicket){
        return this.lotteryTicketService.saveTicket(newTicket);
    }

    @DeleteMapping("/deleteTicket")
    public boolean deleteTicket(@RequestBody LotteryTicket ticket){
        this.lotteryTicketService.deleteTicket(ticket);
        return true;
    }

    @DeleteMapping("/deleteTicketById")
    public boolean deleteTicketById(@RequestBody long ticketId){
        this.lotteryTicketService.deleteTicketById(ticketId);
        return true;
    }

    @PostMapping("/generateTickets")
    public List<LotteryTicket> generateTickets(@RequestBody int amount){
        return this.lotteryTicketService.generateTickets(amount);
    }

    @GetMapping("/checkWin")
    public LotteryTicket checkWin(@RequestBody long ticketId){
        return this.lotteryTicketService.checkWin(ticketId);
    }

    @GetMapping("/active-tickets")
    public List<LotteryTicket> getAllActiveTickets(){
        return this.lotteryTicketService.getAllActiveTickets();
    }

    @PostMapping("/save-all")
    public void saveAllTickets(List<LotteryTicket> tickets){
        this.lotteryTicketService.saveAllTickets(tickets);
    }
}
