package at.spengergasse.lotteryworker.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class LotteryTicket implements Serializable {

    @javax.persistence.Id
    @GeneratedValue
    private Long Id;
    private int[] numbers;
    private Date date;
    private boolean active;
    private int prizeMoney;
}
