package at.spengergasse.lotteryworker.repository;

import at.spengergasse.lotteryworker.model.LotteryTicket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LotteryTicketRepository extends JpaRepository<LotteryTicket,Long> {

    List<LotteryTicket> findByActiveTrue();
}
