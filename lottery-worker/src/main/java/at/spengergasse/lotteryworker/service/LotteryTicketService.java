package at.spengergasse.lotteryworker.service;

import at.spengergasse.lotteryworker.model.LotteryTicket;
import at.spengergasse.lotteryworker.repository.LotteryTicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

@Service
@Transactional
public class LotteryTicketService {

    private final LotteryTicketRepository ticketRepository;

    @Autowired
    public LotteryTicketService(LotteryTicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    public List<LotteryTicket> getAllTickets(){
        return this.ticketRepository.findAll();
    }

    public LotteryTicket saveTicket(LotteryTicket ticket){
        ticket.setDate(new Date());
        ticket.setActive(true);
        return this.ticketRepository.save(ticket);
    }

    public void deleteTicket(LotteryTicket ticket){
        this.ticketRepository.deleteById(ticket.getId());
    }

    public void deleteTicketById(long id){
        this.ticketRepository.deleteById(id);
    }

    public List<LotteryTicket> generateTickets(int amount) {

        List<LotteryTicket> tickets = new ArrayList<>();
        Random random = new Random(System.currentTimeMillis());

        for (int i = 0; i < amount; i++) {
            LotteryTicket ticket = new LotteryTicket();
            ticket.setDate(new Date());
            ticket.setActive(true);
            ticket.setNumbers(IntStream.generate(() -> random.nextInt(44) + 1).limit(6).toArray());
            tickets.add(ticket);
        }

        this.ticketRepository.saveAll(tickets);

        return tickets;
    }

    public LotteryTicket checkWin(long ticketId) {
        return this.ticketRepository.findById(ticketId).get();
    }

    public void saveAllTickets(List<LotteryTicket> tickets) {
        this.ticketRepository.saveAll(tickets);
    }

    public List<LotteryTicket> getAllActiveTickets() {
        return this.ticketRepository.findByActiveTrue();
    }
}
